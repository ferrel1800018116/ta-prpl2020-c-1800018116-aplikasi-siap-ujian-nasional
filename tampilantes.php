<?php 

$pertanyaan = array(

		0 => array(
			'soal' => '“Melalui fasilitas yang disediakan, Doni mengoptimalkan kinerjanya di perusahaan yang telah membesarkan namanya tersebut.”

				1. Inti kalimat di atas ialah…',
		 	'pilihan' => 'A. Fasilitas yang disediakan membuat Doni optimal dalam bekerja |B. Dengan fasilitas yang ada, Doni terus optimal dalam bekerja. |C.  Doni mengoptimalkan kinerjanya. | D. Doni mengoptimalkan kinerjanya di perusahaan.',
		 	'jawaban' => 'C. Doni mengoptimalkan kinerjanya.'
		),
		1 => array(
			'soal' => 'Perhatikanlah kalimat berikut!

				Ais adalah siswa yang rajin belajar. Akan tetapi, ketika ujian ia selalu membuat catatan kecil yang akan ia letakan di laci mejanya sebagai bahan sontekan. Sudah beberapa kali ia mendapatkan teguran dari gurunya, tetapi ia tidak pernah mengakui perbuatannya itu dan menuduh teman sebelahnyalah yang menyontek. Seperti kata peribahasa…

			2. Peribahasa yang sesuai dengan ilustrasi tersebut ialah….',
			'pilihan' => 'A. menawar bunga gugur putik pun gugur |B. air susu dibalas air tuba. |C. habis manis sepah dibuang | D.lempar batu sembunyi tangan.',
			'jawaban' => 'D. lempar batu sembunyi tangan.'
		),
		2 => array(
			'soal' => 'Pemakaian sumber minyak bumi yang sangat besar untuk berbagai keperluan telah menguras sumber-sumber minyak bumi dunia semakin lama semakin berkurang. Memang sampai sekarangpun minyak bumi masih merupakan sumber energi utama sebagai keperluan kehidupan manusia, terutama untuk berbagai macam industri dan transportasi. Jika tidak di temukan sumber energi lain sebagai sumber energi alternatif, dalam waktu tidak lama lagi dunia akan mengalami krisis energi.

			3. Simpulan yang dapat ditarik dari paragraf itu ialah ..',
			'pilihan' => 'A.  Minyak bumi bukan satu-satunya sumber energi |B. Perlunya sumber energi alternatif dalam kehidupan. |C.  Pemakai sumber minyak bumi sangat besar. |D. Dunia akan mengalami krisis energi.',
			'jawaban' => 'D. Dunia akan mengalami krisis energi.'
		),
		3 => array(
			'soal' => 'Pemakaian sumber minyak bumi yang sangat besar untuk berbagai keperluan telah menguras sumber-sumber minyak bumi dunia semakin lama semakin berkurang. Memang sampai sekarangpun minyak bumi masih merupakan sumber energi utama sebagai keperluan kehidupan manusia, terutama untuk berbagai macam industri dan transportasi. Jika tidak di temukan sumber energi lain sebagai sumber energi alternatif, dalam waktu tidak lama lagi dunia akan mengalami krisis energi.

			4. Menurut isinya paragraf di atas berupa',
			'pilihan' => 'A. bahasan |B. kisahan |C. perian |D. paparan',
			'jawaban' => 'A. bahasan'			
		),
		

	);

if(isset($_GET['pilihan'])) {
	$total = 0;

	foreach($_GET['pilihan'] as $a => $b) 
	
		if(substr($b, 0, 1) == $pertanyaan[$a]['jawaban']) 
			$total++;
			echo "
				<script type='text/javascript'>
					alert('Skor anda : ".$total."');
					var jwb = confirm('ingin tes lagi.?');
					if(jwb) {
						window.location ='tampilantes.php';
					} else {
						window.location ='index.html';
					}
				</script>
			";	
}
$no = 1;
$layout = '
	<div>
		<div><br>%s. %s</div>
		<div>%s</div>
	</div>
';
?>

<form>

	<?php


	foreach($pertanyaan as $key => $value) 
	{	
		$pilihan = explode('|', $value['pilihan']);
		$radio = "";
		foreach($pilihan as $h => $i)
		{
			$radio .= '<label><input type="radio" value="'.$i.'" name="pilihan['.$key.']">'.$i. '</label><br>';
		}

		printf($layout, $no++, $value['soal'], $radio);

	}

	?>
	<br><br><br>
<input type="submit" value="Hitung Skor" name="hSkor">
</form>

<?php 

if(isset($_GET['hSkor']) == 'Hitung Skor') {
	echo "
	<script type='text/javascript'>
	alert('Tidak boleh kosong');
	</script>
	";
}

?>